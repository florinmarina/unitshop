<?php

namespace Tests\Unit;

use App\Models\Cart;
use Tests\TestCase;

class CartTest extends TestCase
{

    public function testGetCart()
    {
        $response = $this->call('GET', '/cart');
        $response->assertStatus(200)
            ->assertJson([
                'status' => true,
                "data"   => [
                    "total" => "682.70",
                ],
            ]);
    }

    public function testAddToCartAddProductToCart()
    {
        $response = $this->post('/cart', [
            'product_id' => 1,
            'quantity'   => 1,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }
    public function testAddToCartAddProductToCartFailQuantity()
    {
        $response = $this->post('/cart', [
            'product_id' => 1,
            'quantity'   => 51,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'status'  => false,
                'message' => 'Quantity is bigger then 50',
            ]);
    }

    public function testAddToCartAddProductToCartFailQuantityOnExistedProducts()
    {
        $response = $this->post('/cart', [
            'product_id' => 9,
            'quantity'   => 1,
        ]);

        $cart = Cart::where('product_id', '=', 9)->first();

        $response->assertStatus(200)
            ->assertJson([
                'status'  => false,
                'message' => 'Quantity is bigger then 50. You can add ' . (50 - $cart->quantity) . ' more products.',
            ]);
    }


    public function testAddToCartAddProductToCartFailCount()
    {
        $response = $this->post('/cart', [
            'product_id' => 2,
            'quantity'   => 2,
        ]);

        // check if max 10 different product
        $count = Cart::where('user_id', '=', 1)
            ->where('product_id', '!=', 11)
            ->get()
            ->count();

        $response->assertStatus(200)
            ->assertJson([
                'status'  => false,
                'message' => 'Max 10 products allowed',
            ]);
    }

    // this will run first time ... then you need to make some modifications
    public function testDeleteItemFromCart()
    {
        $response = $this->call('DELETE', '/cart/7');

        $response->assertStatus(200)
            ->assertJson([
                'status'  => true,
                'message' => "Deleted",
            ]);
    }
}
