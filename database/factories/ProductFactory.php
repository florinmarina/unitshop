<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'        => $this->faker->sentence(3),
            'description' => $this->faker->sentence(20),
            'price'       => $this->faker->randomFloat(2, 10, 100),
            'discount'    => $this->faker->randomFloat(2, 1, 10),
        ];
    }
}
