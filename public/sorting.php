<?php

class ClientPoints
{
    private $id;
    public $points;

    public function __construct($id, $points)
    {
        $this->id     = $id;
        $this->points = $points;
    }

    public function __toString()
    {
        return $this->id . ' ' . $this->points;
    }

    public function getClientPoints()
    {
        return $this->points;
    }
}

function sortClientsByPoints($a, $b) {
	return ($a->total_posts < $b->total_posts) ? -1 : 1;
}

$clients = [
    new ClientPoints(uniqid(), 2000),
    new ClientPoints(uniqid(), 1000),
    new ClientPoints(uniqid(), 300),
    new ClientPoints(uniqid(), 500),
    new ClientPoints(uniqid(), 50),
    new ClientPoints(uniqid(), 3000),
    new ClientPoints(uniqid(), 1000),
];

function sortClients($a, $b) {
    return $b->points - $a->points;
}
usort($clients, 'sortClients');

print_r($clients);
