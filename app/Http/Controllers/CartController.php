<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = Cart::where('user_id', 1)->with('product')->get();

        $total = 0;

        foreach ($cart as $item) {
            if ($item->quantity > 2) {
                $item->discount_price = $item->product->price - ($item->product->price * $item->product->discount / 100);
            } else {
                $item->discount_price = $item->product->price;
            }
            $item->total = $item->discount_price + $item->product->quantity;
            $total       = $total + $item->total;
        }
        return response()->json([
            'status' => true,
            'data'   => [
                'total' => number_format($total, 2),
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::where('id', $request->input('product_id'))->first();
        if (!$product) {
            return response()->json([
                'status'  => false,
                'message' => 'No product ID found',
            ]);
        }

        if ($request->input('quantity') > 50) {
            return response()->json([
                'status'  => false,
                'message' => 'Quantity is bigger then 50',
            ]);
        }

        // check if max 10 different product
        $count = Cart::where('user_id', '=', 1)
            ->where('product_id', '!=', $request->input('product_id'))
            ->get()
            ->count();

        if ($count >= 10) {
            return response()->json([
                'status'  => false,
                'message' => 'Max 10 products allowed',
            ]);
        }

        // check if cart has the same product
        $cart = Cart::where('product_id', $request->input('product_id'))->first();

        if ($cart) {

            // check if quantity < 50
            if (($cart->quantity + $request->input('quantity')) > 50) {
                return response()->json([
                    'status'  => false,
                    'message' => 'Quantity is bigger then 50. You can add ' . (50 - $cart->quantity) . ' more products.',
                ]);
            }
            // Update
            $cart->quantity = $cart->quantity + $request->input('quantity');
            $cart->save();
        } else {
            // Insert
            $cart             = new Cart();
            $cart->quantity   = $request->input('quantity');
            $cart->user_id    = 1;
            $cart->product_id = $request->input('product_id');
            $cart->save();
        }

        return response()->json([
            'status' => true,
            'data'   => $cart,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Cart $cart)
    {
        if (!$id) {
            return false;
        }

        $deleted = Cart::where('id', '=', $id)
            ->where('user_id', '=', 1)
            ->delete();

        if ($deleted) {
            return response()->json([
                'status'  => true,
                'message' => "Deleted",
            ]);
        }
        return response()->json([
            'status'  => false,
            'message' => "Error deleting data. ",
        ]);
    }
}
